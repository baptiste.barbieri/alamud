from .effect import Effect2, Effect3
from mud.events import DestroyEvent, Destroy3Event

class DestroyEffect(Effect2):
    EVENT = DestroyEvent

class Destroy3Effect(Effect3):
    EVENT = Destroy3Event
