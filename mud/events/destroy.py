from .event import Event2, Event3


class DestroyEvent(Event2):
    NAME = "destroy"

    def perform(self):
        self.object.move_to(None)
        self.inform("destroy")


class Destroy3Event(Event3):
    NAME = "destroy3"

    def perform(self):
        self.object2.move_to(None)
        self.inform("destroy3")
