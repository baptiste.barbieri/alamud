from .event import Event2

class RemonterEvent(Event2):
    NAME = "remonter"

    def perform(self):
        if not self.object.has_prop("remontable"):
            self.fail()
            return self.inform("remonter.failed")
        return self.inform("remonter")

class AbaisserEvent(Event2):
    NAME = "abaisser"

    def perform(self):
        if not self.object.has_prop("remontable"):
            self.fail()
            return self.inform("abaisser.failed")
        return self.inform("abaisser")



